FILE="Packages/packages-lock.json"
PROJECT="2D"
if [ -f "$FILE" ]
then
    OUTPUT="$(grep com.unity.2d $FILE)"
    if [ "$OUTPUT" == "" ]
    then
        echo "Unity 3D project directory detected."
        PROJECT="3D"
    else
        echo "Unity 2D project directory detected."
    fi
else
    echo "an invalid unity directory detected, exiting."
    exit
fi

while true; do
    read -p "Do you want your assets to exist in the Resources folder? (y/n)" ANSWER
    case $ANSWER in
        [yY])
            mkdir "Assets/Resources"
            mkdir "Assets/Resources/Sprites"
            mkdir "Assets/Resources/Prefabs"
            mkdir "Assets/Resources/Sounds"
            mkdir "Assets/Resources/Sounds/SFX"
            mkdir "Assets/Resources/Sounds/MISC"
            mkdir "Assets/Scripts"
            if [ "$PROJECT" == "3D" ]
            then
                mkdir "Assets/Resources/Textures"
                mkdir "Assets/Resources/Materials"
                mkdir "Assets/Resources/3D Objects"
            fi
            break
            ;;
        [nN])
            mkdir "Assets/Resources"
            mkdir "Assets/Sprites"
            mkdir "Assets/Prefabs"
            mkdir "Assets/Sounds"
            mkdir "Assets/Sounds/SFX"
            mkdir "Assets/Sounds/MISC"
            mkdir "Assets/Scripts"
            if [ "$PROJECT" == "3D" ]
            then
                mkdir "Assets/Textures"
                mkdir "Assets/Materials"
                mkdir "Assets/3D Objects"
            fi
            break
            ;;
        *)
        echo "Please Type Y for Yes or N for No"
    esac
done


